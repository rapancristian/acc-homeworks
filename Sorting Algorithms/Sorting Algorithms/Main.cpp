#include <iostream>
#include <vector>
#include <random>
#include <numeric>
#include <algorithm>
#include <chrono>
#include <fstream>

/* Overloading the << Operator to show the contents of a vector */
template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& v)
{
	os << "[";
	for (int i = 0; i < v.size(); ++i) {
		os << v[i];
		if (i != v.size() - 1)
			os << ", ";
	}
	os << "]\n";
	return os;
}

/* Starts the timer */
void startTimer(std::chrono::steady_clock::time_point& start)
{
	start = std::chrono::high_resolution_clock::now();
}

/* Stops the timer and outputs the result on the screen */
void stopTimer(const std::chrono::steady_clock::time_point& start, const std::string& message)
{
	auto stop = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
	std::cout << message << duration.count() << " milliseconds" << std::endl;
}

/* Creates random numbers and then shuffles them */
void createRandomNumbers(const std::string &fileName)
{
	std::ofstream file(fileName);
	/* Generating the ascending numbers */
	std::vector<int> numbersVec(1000000);
	std::iota(numbersVec.begin(), numbersVec.end(), 0);

	/* Shuffling */
	std::random_device randomDevice;
	std::mt19937 generator(randomDevice());

	auto start = std::chrono::high_resolution_clock::now();
	std::shuffle(numbersVec.begin(), numbersVec.end(), randomDevice);
	stopTimer(start, "Time taken to shuffle: ");

	/* Writing into the txt */
	
	startTimer(start);
	std::shuffle(numbersVec.begin(), numbersVec.end(), randomDevice);

	for (auto& number : numbersVec)
	{
		file << number << std::endl;
	}

	stopTimer(start, "Time taken to write: ");


	file.close();
	
}

/* Populates a vector with the contents of a .txt file*/
void populateVectorFromFile(const std::string &fileName, std::vector<int>& vec, int howMany)
{
	std::ifstream file(fileName);
	int currentNumber;
	for (int i = 0; i < howMany - 1; ++i)
	{
		file >> currentNumber;
		vec.push_back(currentNumber);
	}
	file.close();
}

/* Implementation of bubbles sort */
void bubbleSort(std::vector<int>& vec)
{
	for (int i = 0; i < vec.size(); i++)
	{
		for (int j = 0; j < vec.size() - 1; j++)
		{
			if (vec[j] > vec[j + 1])
				std::swap(vec[j], vec[j + 1]);
		}
	}
}

int main()
{
		std::vector<int> bigNumbersVec;
		auto start = std::chrono::high_resolution_clock::now();
		//createRandomNumbers("numbers.txt");
		startTimer(start);
		populateVectorFromFile("numbers.txt", bigNumbersVec, 1000000);
		stopTimer(start, "Time taken to populate");

		startTimer(start);
		std::sort(bigNumbersVec.begin(), bigNumbersVec.end());
		//bubbleSort(bigNumbersVec);
		stopTimer(start, "Time taken for Std::sort Sort ");
		
	//	std::cout << bigNumbersVec << std::endl;

	std::cin.get();
	return 0;
}